FROM jboss/wildfly

#ADD target/os-demo.war /opt/jboss/wildfly/standalone/deployments/

USER root
RUN yum -y install rsync
USER jboss

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
